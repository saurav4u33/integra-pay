<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              hnhub.com
 * @since             1.0.0
 * @package           Hn_Hub_Integra
 *
 * @wordpress-plugin
 * Plugin Name:       HN Hub Integra Pay
 * Plugin URI:        hnhub.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            HN Hub
 * Author URI:        hnhub.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       hn-hub-integra
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'HN_HUB_INTEGRA_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-hn-hub-integra-activator.php
 */
function activate_hn_hub_integra() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hn-hub-integra-activator.php';
	Hn_Hub_Integra_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-hn-hub-integra-deactivator.php
 */
function deactivate_hn_hub_integra() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hn-hub-integra-deactivator.php';
	Hn_Hub_Integra_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_hn_hub_integra' );
register_deactivation_hook( __FILE__, 'deactivate_hn_hub_integra' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-hn-hub-integra.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_hn_hub_integra() {

	$plugin = new Hn_Hub_Integra();
	$plugin->run();

}
run_hn_hub_integra();
