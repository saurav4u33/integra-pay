<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       hnhub.com
 * @since      1.0.0
 *
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
