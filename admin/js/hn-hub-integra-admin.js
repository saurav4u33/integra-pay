jQuery(document).ready(function($){
	$('.view-invoice').on('click', function(e) {
  		e.preventDefault();
        var pid = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: hnlawajaxdata.ajaxurl,
            context: this,
            data: { action: 'show_payment_invoice', pid : pid },
            success: function(response) {
    			$('.show-invoice').html(response.data);
  				$('.modal').toggleClass('is-visible');
  				cancel_button();
            },
        });
    });
    function cancel_button()
    {
	    $('.show-invoice .dashicons-dismiss').on('click', function(e) {
	  		$('.show-invoice .modal').fadeOut(300, function() { $(this).remove(); });
	  	});
    }
});
