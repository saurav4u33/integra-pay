<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       hnhub.com
 * @since      1.0.0
 *
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/public
 * @author     HN Hub <hnhub@test.com>
 */
class Hn_Hub_Integra_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hn_Hub_Integra_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hn_Hub_Integra_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/hn-hub-integra-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hn_Hub_Integra_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hn_Hub_Integra_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/hn-hub-integra-public.js', array( 'jquery' ), time(), false );
		wp_localize_script(
					$this->plugin_name,
					'hnlawajaxdata',
					array( 'ajaxurl' => admin_url('admin-ajax.php') )
				);
	}

	//function to add front-end form
	function add_front_end_form()
	{	?>
		<form data-integrapay="PaymentForm" method="POST" action="{{action-url}}">
	    	<input data-integrapay="BusinessKey" type="hidden" value="{{business-key}}" />
			<input id="cardCvv" type="hidden" value="000" data-integrapay="CardCcv" />
			<input data-integrapay="CardToken" name="CardToken" type="hidden" />
			<input data-integrapay="CardName" name="CardName" type="text" autocomplete="off" />
			<input data-integrapay="CardNumber" type="text" autocomplete="off" />
			<select data-integrapay="CardExpiryMonth" name="CardExpiryMonth">
			    <option value="01">Jan</option>
			    <option value="02">Feb</option>
			    <option value="03">Mar</option>
			    <option value="04">Apr</option>
			    <option value="05">May</option>
			    <option value="06">Jun</option>
			    <option value="07">Jul</option>
			    <option value="08">Aug</option>
			    <option value="09">Sep</option>
			    <option value="10">Oct</option>
			    <option value="11">Nov</option>
			    <option value="12">Dec</option>
			</select>
			<select data-integrapay="CardExpiryYear" name="CardExpiryYear">
			    <option value="2018">2018</option>
			    <option value="2019">2019</option>
			    <option value="2020">2020</option>
			    <option value="2021">2021</option>
			    <option value="2022">2022</option>
			</select>
			<div data-integrapay="Errors"></div>
			<div data-integrapay="Processing"></div>
			<button data-integrapay="SubmitButton" type="button">Process Payment</button>
    	</form>
	<?php
	}

	//function to add custom form action in the booking form
	function add_custom_booking_form_action()
	{
		return 'google.com'; 
	}

	// function return_handle_form_inputs($EM_Event, $EM_Booking, $post_validation)
	// {
	// 	$token = 'GS4klpRbkURQDoPEbdgUqPK9Ek9WUFsx';
	// 	$curl = curl_init();

	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => "https://api.getgo.com/oauth/v2/token",
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 30,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "POST",
	// 	  CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=".$token,
	// 	  CURLOPT_HTTPHEADER => array(
	// 	    ": ",
	// 	    "Accept: application/json",
	// 	    "Authorization: Basic ZkwweWVENkFveHZFdHYzNTgyRGxqQXBRY0dWQUhqZnE6N2NTdnpIUTlFb1M4elh0dA==",
	// 	    "Content-Type: application/x-www-form-urlencoded",
	// 	  ),
	// 	));

	// 	$response = curl_exec($curl);

	// 	$response = json_decode($response, TRUE);
	// 	$token = $response['access_token'];

	// 	$err = curl_error($curl);

	// 	curl_close($curl);

	// 	if ($err) {
	// 	  echo "cURL Error #:" . $err;
	// 	} 
	// 	else 
	// 	{
	// 		$authorization_key = $token;
	// 		//remains same for a organization
	// 		$organizer_key = "6519891014665704197";
	// 		//webinar key = id of webinar set from event backend
	// 		$webinar_key = '2956713611627161602';//event id 
	// 		$data = array(
	// 		  "firstName" => $EM_Booking->booking_meta['registration']['user_name'],
	// 		  "lastName" => $EM_Booking->booking_meta['registration']['user_name'],
	// 		  "email" => $EM_Booking->booking_meta['registration']['user_email']
	// 		);

	// 		$curl = curl_init();

	// 		curl_setopt_array($curl, array(
	// 		  CURLOPT_URL => "https://api.getgo.com/G2W/rest/v2/organizers/".$organizer_key."/webinars/".$webinar_key."/registrants?resendConfirmation=false",
	// 		  CURLOPT_RETURNTRANSFER => true,
	// 		  CURLOPT_ENCODING => "",
	// 		  CURLOPT_MAXREDIRS => 10,
	// 		  CURLOPT_TIMEOUT => 30,
	// 		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 		  CURLOPT_CUSTOMREQUEST => "POST",
	// 		  CURLOPT_POSTFIELDS => json_encode($data),
	// 		  CURLOPT_HTTPHEADER => array(
	// 		    "Authorization: Bearer ".$authorization_key,
	// 		    "Content-Type: application/json",
	// 		    "accept: application/json",
	// 		  ),
	// 		));

	// 		$response = curl_exec($curl);
	// 		$err = curl_error($curl);

	// 		curl_close($curl);

	// 		if ($err) {
	// 		  echo "cURL Error #:" . $err;
	// 		} 
	// 		print_r($response);
	// 		die;
	// 	}
	// }

	// //function to create a accesstoken
	// function return_access_token_create_connection($EM_Event, $EM_Booking, $post_validation)
	// {
	// 	header("Access-Control-Allow-Origin: *");

	// 	header("Access-Control-Allow-Headers: Content-Type, origin");

	// 	$arr = json_decode("{\n\n    \"ReturnUrl\": \"http://192.168.0.42/hn-website/about-us/\",\n\n    \"Template\": \"Basic\",\n\n    \"Transaction\": {\n\n      \"ProcessType\": \"COMPLETE\",\n\n      \"Reference\": \"HPP-TOKEN-00043\",\n\n      \"Description\": \"Test HPP API Token\",\n\n      \"Amount\": 23.00,\n\n      \"CurrencyCode\": \"AUD\"\n\n    },\n\n    \"Payer\": {\n\n      \"SavePayer\": true,\n\n      \"UniqueReference\": \"HPP-TOKEN-PAYER-U-1\",\n\n      \"GroupReference\": \"HPP-TOKEN-PAYER-GRP-1\",\n\n      \"FamilyOrBusinessName\": \"Surname\",\n\n      \"GivenName\": \"First Name\",\n\n      \"Email\": \"payer.hpp.1@integrapay.com\",\n\n      \"Phone\": \"0733332222\",\n\n      \"Mobile\": \"0411228833\",\n\n      \"Address\": {\n\n        \"Line1\": \"1 Test St\",\n\n        \"Line2\": null,\n\n        \"Suburb\": \"Testville\",\n\n        \"State\": \"QLD\",\n\n        \"PostCode\": \"4001\",\n\n        \"Country\": null\n\n      }\n\n    },\n\n    \"Audit\": {\n\n      \"Username\": \"Token Example\",\n\n      \"UserIP\": \"1.2.3.4\"\n\n    }\n\n  }", true);
	// 	$tnid = rand(5);
	// 	$tnid = 'HPP-TOKEN-'.$tnid;
	// 	// $arr = $arr->Transaction->Reference = $tnid;
	// 	foreach ($arr as $key => $value) {
	// 		if( $value == 'Reference' )
	// 		{
	// 			$arr['Transaction']['Reference'] = $tnid;
	// 		}
	// 	}
	// 	$arr = json_encode($arr,JSON_UNESCAPED_SLASHES);

	// 	$curl = curl_init();
	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => "https://sandbox.auth.paymentsapi.io/login",
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 30,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "POST",
	// 	  CURLOPT_POSTFIELDS => "{\n  \"Username\": 2042.4512,\n  \"Password\": \"d8e3a991-649e-48f3-bd95-d9bff580f150\"\n}",
	// 	  CURLOPT_HTTPHEADER => array(
	// 	    "Accept: */*",
	// 	    "Accept-Encoding: gzip, deflate",
	// 	    "Cache-Control: no-cache",
	// 	    "Connection: keep-alive",
	// 	    "Content-Length: 81",
	// 	    "Content-Type: application/json",
	// 	    "Cookie: __cfduid=d9d797653593b1f3febbeca20b1147d031567744154",
	// 	    "Host: sandbox.auth.paymentsapi.io",
	// 	    "Postman-Token: 3e720b50-a00e-4c76-b590-9d2dbd107e43,dae71129-a9f7-4277-8c58-e2ad54dcbb4c",
	// 	    "User-Agent: PostmanRuntime/7.16.3",
	// 	    "cache-control: no-cache"
	// 	  ),
	// 	));

	// 	$response = curl_exec($curl);
	// 	$err = curl_error($curl);
	// 	$response = json_decode($response, TRUE);
	//     $abc = $response['access_token'];
	//     setcookie('bearer_token', $abc, time() + (3600 * 30), "/"); 
	// 	$def = $this->return_redirect_url_hpp($abc);
	// 	$def = json_decode($def,TRUE);
	// 	$url = $def['redirectToUrl'];
	//  	echo $url;
	//     exit();
	// }


	// //function to get return url
	// function return_redirect_url_hpp($abc)
	// {
	// 	$curl = curl_init();
	// 	$arr = json_decode("{\n\n    \"ReturnUrl\": \"http://192.168.0.42/hn-website/payment-response/\",\n\n    \"Template\": \"Basic\",\n\n    \"Transaction\": {\n\n      \"ProcessType\": \"COMPLETE\",\n\n      \"Reference\": \"HPP-TOKEN-00043\",\n\n      \"Description\": \"Test HPP API Token\",\n\n      \"Amount\": 23.00,\n\n      \"CurrencyCode\": \"AUD\"\n\n    },\n\n    \"Payer\": {\n\n      \"SavePayer\": true,\n\n      \"UniqueReference\": \"HPP-TOKEN-PAYER-U-1\",\n\n      \"GroupReference\": \"HPP-TOKEN-PAYER-GRP-1\",\n\n      \"FamilyOrBusinessName\": \"Surname\",\n\n      \"GivenName\": \"First Name\",\n\n      \"Email\": \"payer.hpp.1@integrapay.com\",\n\n      \"Phone\": \"0733332222\",\n\n      \"Mobile\": \"0411228833\",\n\n      \"Address\": {\n\n        \"Line1\": \"1 Test St\",\n\n        \"Line2\": null,\n\n        \"Suburb\": \"Testville\",\n\n        \"State\": \"QLD\",\n\n        \"PostCode\": \"4001\",\n\n        \"Country\": null\n\n      }\n\n    },\n\n    \"Audit\": {\n\n      \"Username\": \"Token Example\",\n\n      \"UserIP\": \"1.2.3.4\"\n\n    }\n\n  }", true);
	// 	$tnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);;
	// 	$tnid = 'HPP-TOKEN-'.$tnid;
	// 	foreach ($arr as $key => $value) {
	// 		$arr['Transaction']['Reference'] = $tnid;
	// 	}
	// 	$arr = json_encode($arr);
	// 	// $arr = http_build_query($arr);
	// 	// curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => "https://sandbox.rest.paymentsapi.io/businesses/2042/services/tokens/hpp/",
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 30,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "POST",
	// 	  CURLOPT_POSTFIELDS => $arr,
	// 	  CURLOPT_HTTPHEADER => array(
	// 	    "Accept: */*",
	// 	    "Accept-Encoding: gzip, deflate",
	// 	    "Authorization: Bearer ".$abc,
	// 	    "Cache-Control: no-cache",
	// 	    "Connection: keep-alive",
	// 	    "Content-Length: ".strlen($arr),
	// 	    "Content-Type: application/json",
	// 	    "Cookie: __cfduid=d9d797653593b1f3febbeca20b1147d031567744154",
	// 	    "Host: sandbox.rest.paymentsapi.io",
	// 	    "Postman-Token: 5d48bd5b-79de-4ecd-8ef2-34037d83960e,76104046-bd36-4971-a8ff-98b06cb070f0",
	// 	    "User-Agent: PostmanRuntime/7.16.3",
	// 	    "cache-control: no-cache"
	// 	  ),
	// 	));
	// 	$response = curl_exec($curl);
	// 	$err = curl_error($curl);

	// 	curl_close($curl);

	// 	if ($err) {
	// 	  return "cURL Error #:" . $err;
	// 	} else {
	// 	  return $response;
	// 	}
	// }
	// //get payment response
	// function return_payment_response()
	// {

	// 	$curl = curl_init();

	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => "https://sandbox.rest.paymentsapi.io/businesses/2042/services/tokens/",
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 0,
	// 	  CURLOPT_FOLLOWLOCATION => false,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "GET",
	// 	));

	// 	$response = curl_exec($curl);
	// 	$err = curl_error($curl);

	// 	curl_close($curl);

	// 	if ($err) {
	// 	  echo "cURL Error #:" . $err;
	// 	} else {
	// 	  echo $response;
	// 	}
	// }

	// //function to check the payment info via token
	// function return_details_from_token()
	// {
	// 	$token = $_GET['webPageToken'];
	// 	$curl = curl_init();
	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => "https://sandbox.rest.paymentsapi.io/businesses/2042/services/tokens/".$token,
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 0,
	// 	  CURLOPT_FOLLOWLOCATION => false,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "GET",
	// 	  CURLOPT_HTTPHEADER => array(
	// 	    "Accept: */*",
	// 	    "Accept-Encoding: gzip, deflate",
	// 	    "Authorization: Bearer ".get_transient( 'bearer_token' ),
	// 	  ),
	// 	));
	// 	$response = curl_exec($curl);
	// 	$err = curl_error($curl);
	// 	curl_close($curl);

	// 	if ($err) {
	// 	  echo "cURL Error #:" . $err;
	// 	} else {
	// 		$response = json_decode($response);
	// 		echo '<pre>';
	// 		print_r($response);
	// 		echo '</pre>';
	// 	}
	// }
}