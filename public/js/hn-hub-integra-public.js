(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).on('click', '#event-remainder-email-send', function( event ) {
        event.preventDefault();
        var email = $('#event-remainder-email').val();
        if( '' != email ) {
            var data = {
                action  : 'hn_hub_integra_send_ics_to_mail',
                ics     : $(this).data('event'),
                email   : email
            }
            jQuery.ajax({
                url: hnlawajaxdata.ajaxurl,
                type: 'POST',
                data: data,
                dataType: 'JSON',
                async: false,
                beforeSend: function(){
					jQuery('#email-response').html('sending...');
                },
                success: function( response ) {
                    if( !response.success ) {
						jQuery('#email-response').html(  '<p class="ics-error>' + response.data.message + '</p>' );
                       
                    } else {
						jQuery('#email-response').html(  '<p class="ics-success>' + response.data.message + '</p>' );
                    }
                }
            });
        }

    });

})( jQuery );
