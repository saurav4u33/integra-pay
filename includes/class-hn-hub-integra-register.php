<?php
function register_integra_gateway( $gateways ) {
    // Format: ID => Name
    $gateways['integra'] = array(
        'admin_label'    => 'Integrapay',
        'checkout_label' => __( 'Credit Card', 'edds' ),
        'supports'       => array(
            'buy_now'
        )
    );
    return $gateways;
}
add_filter( 'edd_payment_gateways', 'register_integra_gateway' );



add_action( 'edd_gateway_integra', 'edds_process_integra_payment' );
function edds_process_integra_payment( $purchase_data ) {


    $EM_Event[]='';
    $EM_Booking = new stdClass();

    $tnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);;
    $tnid = 'HPP-TOKEN-'.$tnid;
    $extrainfo = json_encode($purchase_data['downloads']);
    $arr = '';
    foreach ($purchase_data['downloads'] as $download) {
        $arr .= $download['id'].',';
    }
    
    // $arr = json_encode($arr);

    $extrainfo = 'Documents-'.$arr;

    #header("Access-Control-Allow-Origin: *");

    #header("Access-Control-Allow-Headers: Content-Type, origin");

    $curl = curl_init();
    curl_setopt_array($curl, array(
     CURLOPT_URL => "https://sandbox.auth.paymentsapi.io/login",
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "",
     CURLOPT_MAXREDIRS => 10,
     CURLOPT_TIMEOUT => 30,
     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     CURLOPT_CUSTOMREQUEST => "POST",
     CURLOPT_POSTFIELDS => "{\n  \"Username\": 2042.4512,\n  \"Password\": \"d8e3a991-649e-48f3-bd95-d9bff580f150\"\n}",
     CURLOPT_HTTPHEADER => array(
       "Accept: */*",
       "Accept-Encoding: gzip, deflate",
       "Cache-Control: no-cache",
       "Connection: keep-alive",
       "Content-Length: 81",
       "Content-Type: application/json",
       "Cookie: __cfduid=d9d797653593b1f3febbeca20b1147d031567744154",
       "Host: sandbox.auth.paymentsapi.io",
       "Postman-Token: 3e720b50-a00e-4c76-b590-9d2dbd107e43,dae71129-a9f7-4277-8c58-e2ad54dcbb4c",
       "User-Agent: PostmanRuntime/7.16.3",
       "cache-control: no-cache"
     ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    $response = json_decode($response, TRUE);
    $abc = $response['access_token'];
    set_transient( 'bearer_token', $abc, 60*60*12 );
    
    
    $curl = curl_init();
    $arr = json_decode("{\n\n    \"ReturnUrl\": \"".esc_url( home_url( '/' ) )."payment-response/\",\n\n    \"Template\": \"Basic\",\n\n    \"Transaction\": {\n\n      \"ProcessType\": \"COMPLETE\",\n\n      \"Reference\": \"\",\n\n      \"Description\": \"Test HPP API Token\",\n\n      \"Amount\": 23.00,\n\n      \"CurrencyCode\": \"AUD\"\n\n    },\n\n    \"Payer\": {\n\n      \"SavePayer\": false,\n\n      \"FamilyOrBusinessName\": \"\",\n\n      \"GivenName\": \"First Name\",\n\n      \"Email\": \"\",\n\n      \"Phone\": \"\",\n\n      \"Mobile\": \"\",\n\n      \"Address\": {\n\n        \"Line1\": \"\",\n\n        \"Line2\": null,\n\n        \"Suburb\": \"\",\n\n        \"State\": \"\",\n\n        \"PostCode\": \"\",\n\n        \"Country\": null\n\n      }\n\n    },\n\n    \"Audit\": {\n\n      \"Username\": \"Token Example\",\n\n      \"UserIP\": \"\"\n\n    }\n\n  }", true);
    $tnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);;
    $tnid = 'HPP-TOKEN-'.$tnid;
    // pr( $purchase_data );
    foreach ($arr as $key => $xyz) {
        if(is_array($xyz))
        {
            foreach ($xyz as $k => $v) {
                if( $k == 'Reference' )
                {
                    $arr['Transaction']['Reference'] = $tnid;
                }
                if( $k == 'Amount' )
                {
                    $arr['Transaction']['Amount'] = $purchase_data['price'];
                }
                $arr['Payer']['GivenName'] = $purchase_data['user_info']['first_name'];
                #$arr['Payer']['FamilyOrBusinessName'] = $EM_Booking->booking_meta['booking']['company_name'];
                $arr['Payer']['FamilyOrBusinessName'] = $purchase_data['user_info']['last_name'];
                $arr['Payer']['Email'] = $purchase_data['user_info']['email'];
                #$arr['Payer']['Mobile'] = $EM_Booking->booking_meta['registration']['dbem_phone'];
                $arr['Payer']['Mobile'] = '';
                
                $arr['Payer']['Address']['Line1'] = $purchase_data['user_info']['address']['line1'];
                $arr['Payer']['Address']['Suburb'] = $purchase_data['user_info']['address']['city'];
                $arr['Payer']['Address']['PostCode'] = $purchase_data['user_info']['address']['zip'];
                $arr['Payer']['Address']['Country'] = $purchase_data['user_info']['address']['country'];
                $arr['Payer']['Address']['State'] = $purchase_data['user_info']['address']['state'];
                $arr['Payer']['Address']['Line2'] = $purchase_data['user_info']['address']['line2'];
                $arr['Transaction']['Description'] = $extrainfo;
            }
        }
    }
    $arr = json_encode($arr);
    curl_setopt_array($curl, array(
     CURLOPT_URL => "https://sandbox.rest.paymentsapi.io/businesses/2042/services/tokens/hpp/",
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "",
     CURLOPT_MAXREDIRS => 10,
     CURLOPT_TIMEOUT => 30,
     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     CURLOPT_CUSTOMREQUEST => "POST",
     CURLOPT_POSTFIELDS => $arr,
     CURLOPT_HTTPHEADER => array(
       "Accept: */*",
       "Accept-Encoding: gzip, deflate",
       "Authorization: Bearer ".$abc,
       "Cache-Control: no-cache",
       "Connection: keep-alive",
       "Content-Length: ".strlen($arr),
       "Content-Type: application/json",
       "Cookie: __cfduid=d9d797653593b1f3febbeca20b1147d031567744154",
       "Host: sandbox.rest.paymentsapi.io",
       "Postman-Token: 5d48bd5b-79de-4ecd-8ef2-34037d83960e,76104046-bd36-4971-a8ff-98b06cb070f0",
       "User-Agent: PostmanRuntime/7.16.3",
       "cache-control: no-cache"
     ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    /////////////////////////////
    $def = json_decode($response,TRUE);
    $url = $def['redirectToUrl'];
    wp_redirect($url);
    exit();
}
