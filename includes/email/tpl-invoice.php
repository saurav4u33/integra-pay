<?php

// $my_html = '
// <!DOCTYPE html>
// <html>
// <head>
//     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
// </head>
// <body>
//     <div class="invoice-box">
//         <table cellpadding="0" cellspacing="0">
//             <tr class="top">
//                 <td colspan="2">
//                     <table>
//                         <tr>
//                             <td class="title">
//                                 <img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191128173245.png" alt="holley nethercote" />
//                             </td>
                            
//                             <td>
//                                 HNLaw Pty Ltd<br>
//                                 ACN 068 367 046<br>
//                                 Level 22, 140 William Street <br>
//                                 MELBOURNE VIC 3000<br>
//                                 GPO BOX 3045<br>
//                                 MELBOURNE VIC 3001<br>
//                                 T: +61 3 9670 8200<br>
//                                 E: <a href="mailto:info@hnlaw.com.au">info@hnlaw.com.au</a><br>
//                                 W: <a href="www.hnlaw.com.au">www.hnlaw.com.au</a>
//                             </td>
//                         </tr>
//                     </table>
//                 </td>
//             </tr>
//             <tr class="tax-title">
//                 <td colspan="2">
//                     <table>
//                         <tr>
//                             <td class="title-abn" align="left">
//                                 ABN 30 339 960 335
//                             </td> 
//                         </tr>
//                         ';
//                     if(!isset($response_message) ) {

//                     $my_html .= '
//                         <tr>
//                             <td class="title-tax" align="center">
//                             <strong>TAX INVOICE</strong>
//                             </td> 
//                         </tr>';
//                     }

//                     $my_html .= '  
//                      </table>
//                 </td>
//             </tr>

//             <tr class="information">
//                 <td colspan="2">
//                     <table>
//                         <tr>
//                             <td>' . $response_firstname . '<br />' . $response_email . '</td>
//                             <td>';
//                                 if( $response_reference && '' != $response_reference ){

//                                     $my_html .= 'Invoice : <strong>' . $response_reference . '</strong><br>';
//                                 }
                                
//                                 if( $response_date && '' != $response_date ){
//                                     $my_html .= 'Date : <span>' . $response_date . '</span><br>';
//                                 }       
//                         $my_html .= '
//                             </td>
//                         </tr>
//                         <tr>
//                             <td colspan="2"><strong>' . $response_title . '</strong></td>
//                         </tr>
//                     </table>
//                 </td>
//             </tr> ';           

//     if( $response_items ) {
//         $my_html .= '
//             <tr class="heading">
//                 <td>
//                     &nbsp;
//                 </td>
                
//                 <td>
//                     Amount
//                 </td>
//             </tr>';
//             foreach( $response_items as $key => $item ) {
//                 $my_html .= '<tr class="' . ( strpos($key,'item' ) ? 'item': $key ) . '">
//                                 <td>' . $item['title'] . '</td>
                                
//                                 <td>' .$item['value'] . '</td>
//                             </tr>';
//             }
//             $my_html .= '<tr>
//                 <td colspan="2"><p>Note : This invoice is for your records.</p></td>
//                 </tr>';
//     }

//     if( isset( $response_message ) && $response_message ) {
//             $my_html .= '<tr class="information">
//             <td colspan="2">
//                 <table>
                
//                     <tr>
//                         <td colspan="2"></td>
//                     </tr>
//                 </table>
//             </td>
//         </tr>';
//     } 



//     $my_html .= '</table>
//     </div>
// </body>
// </html>';


$my_html = '<!DOCTYPE html>
            <html>
                <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                   
                </head>
                <body>
                   <div class="invoice-box">
                       <table cellpadding="0" cellspacing="0">
                           <tr class="top">
                               <td colspan="2">
                                   <table>
                                       <tr>
                                           <td class="title">
                                               <img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191128173245.png" alt="holley nethercote">
                                           </td>
                                           
                                           <td>
                                               HNLaw Pty Ltd<br>
                                               ACN 068 367 046<br>
                                               Level 22, 140 William Street <br>
                                               MELBOURNE VIC 3000<br>
                                               GPO BOX 3045<br>
                                               MELBOURNE VIC 3001<br>
                                               T: +61 3 9670 8200<br>
                                               E: <a href="mailto:info@hnlaw.com.au">info@hnlaw.com.au</a><br>
                                               W: <a href="www.hnlaw.com.au">www.hnlaw.com.au</a>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>
                           <tr class="tax-title">
                               <td colspan="2">
                                   <table>
                                       <tr>
                                           <td class="title-abn" align="left">
                                               ABN 30 339 960 335
                                           </td> 
                                       </tr>
                                       <tr>
                                           <td class="title-tax" align="center">
                                               <strong>TAX INVOICE</strong>
                                           </td> 
                                       </tr>
                                   </table>
                               </td>
                           </tr>
                           
                           <tr class="information">
                               <td colspan="2">
                                   <table>
                                       <tr>
                                           <td>' . $response_firstname . '<br>' . $response_email . '</td>
                                           <td>
                                               Invoice : <strong>' . $response_reference . '</strong><br>
                                               Date : <span>' . $response_date . '</span><br>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td colspan="2"><strong>Re : Event registration invoice</strong></td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>            
                           <tr class="heading">
                               <td>
                                   &nbsp;
                               </td>
                               
                               <td>
                                   Amount
                               </td>
                           </tr>';
                           
                            foreach( $response_items as $key => $item ) {
                                $my_html .= '<tr class="' . ( strpos($key,'item' ) ? 'item': $key ) . '">
                                <td>' . $item['title'] . '</td>
                                <td>' .$item['value'] . '</td>
                                </tr>';
                            }
                            $my_html .= '<tr><td>Amount Due:</td><td>$0.00</td></tr>';
                            $my_html .= '<tr>
                               <td colspan="2"><p>Note : This invoice is for your records.</p></td>
                           </tr>
                       </table>
                   </div>
                </body>
            </html>';

            echo $my_html;