<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       hnhub.com
 * @since      1.0.0
 *
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/includes
 * @author     HN Hub <hnhub@test.com>
 */
class Hn_Hub_Integra_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'hn-hub-integra',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
