<?php
#require_once get_stylesheet_directory().'/vendor/autoload.php';

class Hn_Hub_Integra_Handle_Payment {
    function init() {
      add_action('em_bookings_added', array($this, 'return_access_token_create_connection'), 11, 1);
      // add_action( 'em_bookings_added', array( $this, 'em_bookings_added_cb' ), 11, 1 );
      add_shortcode('Get_Payment_Response', array($this, 'return_details_from_token'));
      add_action('send_email_invoice', array($this, 'send_email_invoice'), 10, 1);
      add_action('wp_ajax_hn_hub_integra_send_ics_to_mail', array($this, 'hn_hub_integra_send_ics_to_mail'));
      add_action('wp_ajax_nopriv_hn_hub_integra_send_ics_to_mail', array($this, 'hn_hub_integra_send_ics_to_mail'));
    }

    function send_email_invoice($response) {
        // INCLUDE THE phpToPDF.php FILE
        $extrainfo = $response['requestHpp']['transaction']['description'];
        $email_send_group = $my_html = false;
        if (strpos($extrainfo, 'Event') !== false) {
            $email_send_group = 'event';
            $desc = explode("-", $extrainfo);
            $did = $desc[1];
            $event_id = $did;
            $eid = get_post_meta($did, '_event_id', true);
            global $wpdb;
            $results = $wpdb->get_results("SELECT ticket_price FROM hnlaw_em_tickets WHERE event_id=$eid", OBJECT);
            $output = number_format((float)$results[0]->ticket_price, 2, '.', '');
            $tax_rate = get_option('dbem_bookings_tax');
            $abcid = get_transient('bookingid');
            $bmeta = $wpdb->get_results( "SELECT `booking_meta` FROM hnlaw_em_bookings WHERE booking_id=$abcid", OBJECT );
            $isValidJson = maybe_unserialize($bmeta[0]->booking_meta);
            $discount = isset( $isValidJson['coupon']['coupon_discount'] ) ? $isValidJson['coupon']['coupon_discount']:'';
            $total = $output;
            if ( isset($discount) && $discount!='' ) {
                if ($isValidJson['coupon']['coupon_type'] == '%') {
                    $coupon_type = '%';
                    $total = $total - $discount / 100 * $total;
                    $discount = $discount / 100 * $total;
                }
                if ($isValidJson['coupon']['coupon_type'] == '#') {
                    $coupon_type = '$';
                    $total = $total - $discount;
                }
            }
            $tax = ($tax_rate / 100) * $total;
            // echo $tax.'='.$tax_rate.'='.$total;
            $subtotal = $total;

            if(isset($discount) && $discount!='')
            {
              $subtotal = $output - $discount;
            }
            
            // PUT YOUR HTML IN A VARIABLE
            $response_firstname = $response['requestHpp']['payer']['givenName'];
            $response_email = $response['requestHpp']['payer']['email'];
            $response_reference = $response['requestHpp']['transaction']['reference'];
            $response_date = date("F j, Y");
            $response_title = 'Re : Event registration invoice';
            $response_items = array();
            $response_items['item1'] = array( 
                'title' => get_the_title($event_id) ,
                'value' => '$'. number_format((float)$output, 2, '.', '')
            ); 
            $discount_format = (isset($discount) && $discount!='') ? '('.$coupon_type.$discount.')':'';
            $response_items['item2'] = array( 
                'title' => 'Less Discount:'.$discount_format,
                'value' => isset( $coupon_type ) ? $coupon_type.number_format((float)$discount, 2, '.', ''):'-',
            ); 
            $response_items['item3'] = array( 
                'title' => 'Amount:',
                'value' => ' $'. number_format((float)$subtotal, 2, '.', ''),
            );
            $response_items['item4'] = array( 
                'title' => '+ GST:' ,
                'value' => '$' . number_format((float)$tax, 2, '.', '')
            );
            $response_items['total'] = array( 
                'title' => 'Total:' ,
                'value' => '$' . number_format((float)$response['transaction']['amount'], 2, '.', '')
            );
            require  __DIR__.'/email/tpl-invoice.php';
            
            $prefix = str_replace('-', '', $response['transaction']['reference']);
            $res = preg_replace("/[^0-9]/", "", $prefix);
            $filename = 'Invoice-' . $res . '.pdf';
            // ---------------------------------------------------------
            $upload = wp_upload_dir();
            $upload_dir = $upload['basedir'] . '/invoices';
            $fullfilepath = $upload_dir . '/' . $filename;
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, 0700);
            }
            $sdate = get_post_meta($event_id, '_event_start_date', true);
            $edate = get_post_meta($event_id, '_event_end_date', true);
            $stime = get_post_meta($event_id, '_event_start_time', true);
            $etime = get_post_meta($event_id, '_event_end_time', true);
            $location_id = get_post_meta($event_id, '_location_id', true);
            global $wpdb;
            $results = $wpdb->get_results("SELECT * FROM hnlaw_em_locations WHERE location_id=$location_id", OBJECT);

            $webinar_url = get_field('webinar_key', $event_id);
            $locate = 'Webinar';
            if (isset($results) && $results[0]->location_name != '') {
                $locate = esc_attr($results[0]->location_name);
            } 
            $hear = $wpdb->get_results("SELECT `booking_meta` FROM `hnlaw_em_bookings` WHERE `booking_id` = $abcid", OBJECT);
            $hear = maybe_unserialize($hear[0]->booking_meta);
            if(isset($hear) && isset($hear['booking']['hear']) && $hear['booking']['hear']!='' && $hear['booking']['hear']!='Other')
            {
              $hear = $hear['booking']['hear'];
            }
            else{
              $hear = $hear['booking']['other'];
            }
            $message = get_field('event_message_body_user', 'option');
            $message = str_replace('{user}', $response['requestHpp']['payer']['givenName'], $message);
            $message = str_replace('{event}', get_the_title($desc[1]), $message);
            $message = str_replace('{date}', date("d M Y", strtotime($sdate)), $message);
            $message = str_replace('{time}', $stime, $message);
            $message = str_replace('{location}', $locate, $message);
            $message = str_replace('{hear}', $hear, $message);
            if (isset($webinar_url) && $webinar_url != '') {
                $message .= '<p style="font-size: 15px;"><strong>IMPORTANT – One final step to complete your Registration</strong></p><p>Your registration is almost complete. Please click on the link below to register with GotoWebinar. You will then be emailed login details for the webinar.</p>' . '<p><a style="text-decoration: underline;" href="https://attendee.gotowebinar.com/register/' . $webinar_url . '"><b> https://attendee.gotowebinar.com/register/' . $webinar_url . '</b></a></p>';
            }
            $message .= '<p style="font-size: 15px;"><strong>CHANGES & CANCELLATIONS</strong></p><p>If you need to change or cancel your registration, please call (03) 9670 8200 or email <a href="mailto:info@hnlaw.com.au">info@hnlaw.com.au</a></p> 
                       <p><a href="https://hnlaw.com.au/terms-conditions/">Cancellation policy</a></p>
                       <p>We look forward to seeing you.</p>';
            $ticketid = $wpdb->get_results("SELECT * FROM hnlaw_em_tickets WHERE event_id=$eid", OBJECT);
            $totalspaces = $ticketid[0]->ticket_spaces;
            $status_cond = !get_option('dbem_bookings_approval') ? 'booking_status IN (0,1)' : 'booking_status = 1';
            $sql = 'SELECT SUM(booking_spaces) FROM ' . EM_BOOKINGS_TABLE . " WHERE $status_cond AND event_id=" . absint($eid);
            $booked_spaces = $wpdb->get_var($sql);
            $remaining = $totalspaces - $booked_spaces;
           
            $message1 = get_field('event_message_body_admin', 'option');
            $message1 = str_replace('{event}', get_the_title($desc[1]), $message1);
            $message1 = str_replace('{date}', date("d M Y", strtotime($sdate)), $message1);
            $message1 = str_replace('{time}', $stime, $message1);
            $message1 = str_replace('{location}', $locate, $message1);
            $message1 = str_replace('{name}', $response['requestHpp']['payer']['givenName'], $message1);
            $message1 = str_replace('{email}', $response['payer']['email'], $message1);
            $message1 = str_replace('{reference}', $response['transaction']['reference'], $message1);
            $message1 = str_replace('{bookingid}', get_transient('bookingid'), $message1);
            $message1 = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message1);
            $message1 = str_replace('{remaining}', $remaining, $message1);
            $message1 = str_replace('{hear}', $hear, $message1);
            
            $message1 .=  '<table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';
            $body = "<!-- INTRO : BEGIN -->
                    <tr>"
                     .$message.   
                    "</tr>
                    <!-- INTRO : END -->";
            $message = get_edm_layout($body); 
        } else if (strpos($extrainfo, 'Documents') !== false) {
            $email_send_group = 'document';
            $docs = '';
            $extrainfo1 = substr( $extrainfo, strpos($extrainfo, "-") + 1 );
            if (strpos($extrainfo1, ',') !== false) {
                $total = 0;
                $extrainfo1 = rtrim($extrainfo1, ',');
                if (strpos($extrainfo1, ',') !== false) {
                    $extrainfo1 = explode(',', $extrainfo1);
                    $docs = '';
                    foreach ($extrainfo1 as $key) {
                        $docs.= '<p>'.get_the_title($key).'</p>';
                        $total+= get_post_meta($key, 'edd_price', true);
                    }
                } else {
                    $docs.= '<p>'.get_the_title($extrainfo1).'</p>';
                    $total+= get_post_meta($extrainfo1, 'edd_price', true);
                }
            }
            $message = '<b>Dear ' . $response['requestHpp']['payer']['givenName'] . ',</b><p>
                    Thank you for your purchase. An invoice confirming payment is attached.</p>
                       <div class="event-details">
                       <p style="font-size: 15px;"><strong>Document(s):</strong></p>
                         <p style="margin-left:20px; padding:0px;"><b style:"width: 70px;">Name: </b>' . $docs . '</p>
                         <p style="margin-left:20px; padding:0px;"><b style:"width: 70px;">Amount: $</b>' . $response['transaction']['amount'] . '</p>
                       </div>
                   <div>';

            $message  = get_field('document_message_body_user', 'option');
            $message  = str_replace('{user}', $response['requestHpp']['payer']['givenName'], $message);
            $message  = str_replace('{docs}', $docs, $message);
            $message  = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message);
            $message .=  '
                           <table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';

            $body = "<!-- INTRO : BEGIN -->
                    <tr>"
                     .$message.   
                    "</tr>
                    <!-- INTRO : END -->";
            $message = get_edm_layout($body); 
            $edd_settings = get_option('edd_settings');
            $tax_rate = edd_get_option( 'tax_rate', 0 );
            $tax = $tax_rate / 100 * $total;

            $response_firstname = $response['requestHpp']['payer']['givenName'];
            $response_email = $response['requestHpp']['payer']['email'];
            $response_reference = $response['requestHpp']['transaction']['reference'];
            $response_date = date("F j, Y");
            $response_title = 'Re : Document(s) purchase invoice';

            $response_items = array();
            $response_items['item1'] = array( 
                'title' => ' Document(s)',
                'value' => $docs
            ); 
            $response_items['sub-total'] = array( 
                'title' => 'Sub Total:' ,
                'value' => '$' . number_format((float)$total, 2, '.', '')
            ); 
            $response_items['item2'] = array( 
                'title' => 'GST @' . $tax_rate,
                'value' => '$'. number_format((float)$tax, 2, '.', '')            );
            $response_items['total'] = array( 
                    'title' => 'Total:',
                    'value' => '$' . number_format((float)$response['transaction']['amount'], 2, '.', '')
            ); 
            require  __DIR__.'/email/tpl-invoice.php';

            $message1 = get_field('document_message_body_admin', 'option');
            $message1 = str_replace('{docs}', $docs, $message1);
            $message1 = str_replace('{reference}', $response['transaction']['reference'], $message1);
            $message1 = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message1);
            $message1 .=  '
                           <table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';
        } else if (strpos($extrainfo, 'Course') !== false) {
            $email_send_group = 'course';
            
            $extrainfo1 = substr($extrainfo, strpos($extrainfo, "-") + 1);
            $course = get_the_title($extrainfo1);
            update_user_meta(get_current_user_id(), "course_{$extrainfo1}_access_from", time());
            $message = '<b>Dear ' . $response['requestHpp']['payer']['givenName'] . ',</b><p>
                    Thank you for your purchase. An invoice confirming payment is attached.</p>
                       <div class="event-details">
                       <p style="font-size: 15px;"><strong>Course(s):</strong></p>
                         <p style="margin-left:20px; padding:0px;"><b style:"width: 70px;">Name: </b>' . $course . '</p>
                         <p style="margin-left:20px; padding:0px;"><b style:"width: 70px;">Amount: $</b>' . $response['transaction']['amount'] . '</p>
                       </div>
                   <div>';
            $message = get_field('course_message_body_user', 'option');
            $message = str_replace('{user}', $response['requestHpp']['payer']['givenName'], $message);
            $message = str_replace('{course}', $course, $message);
            $message = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message);
            $message .= '<table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';
            $body = "<!-- INTRO : BEGIN -->
                    <tr>"
                     .$message.   
                    "</tr>
                    <!-- INTRO : END -->";
            $message = get_edm_layout($body); 
            $response_firstname = $response['requestHpp']['payer']['givenName'];
            $response_email = $response['requestHpp']['payer']['email'];
            $response_reference = $response['requestHpp']['transaction']['reference'];
            $response_date = date("F j, Y");
            $response_title = 'Re : Course(s) purchase invoice';
            $response_items = array();
            $response_items['item1'] = array( 
                'title' => 'Course(s)',
                'value' => $course
            ); 
            
            $response_items['total'] = array( 
                    'title' => '&nbsp;' ,
                    'value' => 'Total: $' . number_format((float)$response['transaction']['amount'], 2, '.', '')
            ); 
            require  __DIR__.'/email/tpl-invoice.php';

           
            $message1 = get_field('course_message_body_admin', 'option');
            $message1 = str_replace('{course}', $course, $message1);
            $message1 = str_replace('{reference}', $response['transaction']['reference'], $message1);
            $message1 = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message1);

            $message1 .=  '<table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';
        } else if (strpos($extrainfo, 'Webinar') !== false) {
            $email_send_group = 'webinar';
           
            $extrainfo1 = substr($extrainfo, strpos($extrainfo, "-") + 1);
            $webinar = get_the_title($extrainfo1);
            
            $message = get_field('webinar_message_body_user', 'option');
            $message = str_replace('{user}', $response['requestHpp']['payer']['givenName'], $message);
            $message = str_replace('{webinar}', $webinar, $message);
            $message = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message);
            $message .= '<table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';
            $body = "<!-- INTRO : BEGIN -->
                    <tr>"
                     .$message.   
                    "</tr>
                    <!-- INTRO : END -->";
            $message = get_edm_layout($body); 
            $response_firstname = $response['requestHpp']['payer']['givenName'];
            $response_email = $response['requestHpp']['payer']['email'];
            $response_reference = $response['requestHpp']['transaction']['reference'];
            $response_date = date("F j, Y");
            $response_title = 'Re : Webinar purchase invoice';
            $response_items = array();
            $response_items['item1'] = array( 
                'title' => 'Webinar',
                'value' => $webinar
            ); 
            
            $response_items['total'] = array( 
                    'title' => '&nbsp;' ,
                    'value' => 'Total: $' . number_format((float)$response['transaction']['amount'], 2, '.', '')
            ); 
            require  __DIR__.'/email/tpl-invoice.php';
           
            $message1 = get_field('webinar_message_body_admin', 'option');
            $message1 = str_replace('{webinar}', $webinar, $message1);
            $message1 = str_replace('{reference}', $response['transaction']['reference'], $message1);
            $message1 = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message1);

            $message1 .=  '<table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';
        } else if (strpos($extrainfo, 'Package') !== false) {
            
            $email_send_group = 'package';
           
            $extrainfo1 = substr($extrainfo, strpos($extrainfo, "-") + 1);
            $desc = explode("-", $extrainfo);
            $description = $desc[0];
            $docs = $desc[1];
            $order_id = get_transient('order_id');
            $sub_user_count = get_post_meta($order_id, 'users__number_of_users', true);
           
            $message = get_field('package_message_body_user', 'option');
            $message = str_replace('{user}', $response['requestHpp']['payer']['givenName'], $message);
            $message = str_replace('{package}', $docs, $message);
            $message = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message);
            $message .= '<table width="600" border="0" style="margin:0px; padding:0px;">
                       <tr>
                           <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                       </tr>
                       <tr>
                           <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                       </tr>
                       <tr>
                           <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                have received this message in error, please delete this email and contact us on the details above.</p></td>
                       </tr>
                       <tr>
                           <td>&nbsp;</td>
                       </tr>
                    </table>
               </div>';
            $body = "<!-- INTRO : BEGIN -->
                    <tr>"
                     .$message.   
                    "</tr>
                    <!-- INTRO : END -->";
            $message = get_edm_layout($body); 
            $response_firstname = $response['requestHpp']['payer']['givenName'];
            $response_email = $response['requestHpp']['payer']['email'];
            $response_reference = $response['requestHpp']['transaction']['reference'];
            $response_date = date("F j, Y");
            $response_title = 'Re : Package subscription invoice';
            $response_items = array();
            $response_items['item1'] = array( 
            'title' => 'Package(s)',
            'value' => $docs
            ); 
            $response_items['item2'] = array( 
            'title' => 'Order Id' ,
            'value' => get_transient('order_id')
            );
            $response_items['item3'] = array( 
            'title' => 'Number of Sub-users',
            'value' => $sub_user_count
            ); 
            $response_items['total'] = array( 
                'title' => '&nbsp;' ,
                'value' => 'Total: $' . number_format((float)$response['transaction']['amount'], 2, '.', '')
            ); 
            
            require  __DIR__.'/email/tpl-invoice.php';

           
            $message1 = get_field('package_message_body_admin', 'option');
            $message1 = str_replace('{user}', $response['requestHpp']['payer']['givenName'], $message1);
            $message1 = str_replace('{email}', $response['requestHpp']['payer']['email'], $message1);
            $message1 = str_replace('{invoice}', $response['transaction']['reference'], $message1);
            $message1 = str_replace('{date}', date('F j, Y'), $message1);
            $message1 = str_replace('{package}', $docs, $message1);
            $message1 = str_replace('{sub_user}', $sub_user_count, $message1);
            $message1 = str_replace('{reference}', $response['transaction']['reference'], $message1);
            $message1 = str_replace('{amount}', '$'.number_format((float)$response['transaction']['amount'], 2, '.', ''), $message1);

            $message1 .=  '<table width="600" border="0" style="margin:0px; padding:0px;">
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                               </tr>
                               <tr>
                                   <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                               </tr>
                               <tr>
                                   <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                        If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                        have received this message in error, please delete this email and contact us on the details above.</p></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                               </tr>
                            </table>
                       </div>';
        }
        
        $prefix = str_replace('-', '', $response['transaction']['reference']);
        
        $res = preg_replace("/[^0-9]/", "", $prefix);
        
        $filename = 'Invoice-' . $res . '-' . time().'.pdf';
        // ---------------------------------------------------------
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'] . '/invoices';
        $fullfilepath = $upload_dir . '/' . $filename;
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0755);
        }
    
        $stylesheet = '' . file_get_contents(get_template_directory_uri() . '/inc/pdf.css') . '';
        try{
            $mpdf = new \Mpdf\Mpdf();
            
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
    
            $mpdf->use_kwt = true;
            $mpdf->AddPage('P', //orientation L - landscape, P - portrait
            '', // type = E|O|even|odd|next-odd|next-even
            '', // resetpagenum = 1 - ∞
            '', // pagenumstyle = 1|A|a|I|i
            '', // suppress = on|off|1|0
            3, // margin_left
            3, // margin right
            3, // margin top
            3, // margin bottom
            0, // margin header
            0); // margin footer
            
            $mpdf->WriteHTML($my_html,\Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($fullfilepath, 'F');
        
        } catch (\Mpdf\MpdfException $e) { 
            echo $e->getMessage();
        }
       
        
        $data = chunk_split(base64_encode(file_get_contents($fullfilepath)));
        
        $headers = 'FROM: info@hnlaw.com.au' . "\r\n";

        if( $email_send_group ) {
            $mailit = wp_mail(
              $response['requestHpp']['payer']['email'], 
              get_field( $email_send_group .'_subject_user', 'option'). ' Invoice',
              $message, 
              $headers, 
              array( $fullfilepath )
             );
            $admin_mail = get_option('admin_email');
            $mailadmin = wp_mail($admin_mail, get_field( $email_send_group . '_subject_admin', 'option'), $message1, $headers);
        }
        update_option($response['transaction']['reference'], $filename, true);
    }

    function get_oauth_access_token_from_api_getgo(){
        $client_key = 'dVPlbJC8jGQWZPeOhNac5AdK2GjRld6M';
        $client_secret = 'AuuWITdmeghWjkN8';
        $endpoint = 'https://api.getgo.com/oauth/v2/token';

        $curl = curl_init();
        curl_setopt_array(
            $curl, 
            array(
                CURLOPT_URL => $endpoint, 
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "", 
                CURLOPT_MAXREDIRS => 10, 
                CURLOPT_TIMEOUT => 30, 
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
                CURLOPT_CUSTOMREQUEST => "POST", 
                CURLOPT_POSTFIELDS => "grant_type=password&username=info%40hnlaw.com.au&password=%23hnadminlogin01", 
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json", 
                    "Accept-Encoding: gzip, deflate", 
                    "Authorization: Basic " . base64_encode($client_key . ':' . $client_secret), 
                    "Content-Type: application/x-www-form-urlencoded", 
                    "Host: api.getgo.com"
                ),
            )
        );
        $result = curl_exec($curl);

        $err = curl_error($curl);
        
        curl_close($curl);

        return array( $result, $err );
        
    }

    function return_handle_form_inputs(&$response) {
        list( $result, $error )= $this->get_oauth_access_token_from_api_getgo();
        
        if (!$error) {
            if ($result) {
                $response1 = json_decode($result, TRUE);
                $authorization_key = $response1['access_token'];
                $organizer_key = $response1['organizer_key'];

                $extrainfo = $response['payer']['address']['suburb'];
                if (strpos($extrainfo, 'Event') !== false) {
                    $desc = explode("-", $extrainfo);
                    $webinar_key = $desc[1]; //event id
                    $description = $desc[0];
                    $data = array("firstName" => $response['transaction']['payer']['givenName'], "lastName" => $response['transaction']['payer']['familyOrBusinessName'], "email" => $response['transaction']['payer']['email']);
                    $curl = curl_init();
                    curl_setopt_array($curl, array(CURLOPT_URL => "https://api.getgo.com/G2W/rest/v2/organizers/" . $organizer_key . "/webinars/" . $webinar_key . "/registrants?resendConfirmation=false", CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => json_encode($data), CURLOPT_HTTPHEADER => array("Authorization: Bearer " . $authorization_key, "Content-Type: application/json", "accept: application/json",),));
                    $response2 = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                        echo "cURL Error #:" . $err;
                    }
                }

            }
        } else {
            echo "Error" . $err;
        }
    }

    //function to create a accesstoken
    function return_access_token_create_connection($EM_Booking) {
        global $EM_Event;
        set_transient('bookingid', $EM_Booking->booking_id, time() + (3600 * 30));
        if ($EM_Booking->booking_meta['gateway'] == 'offline') {
            $did = $EM_Booking->event_id;
            $event_id = $EM_Booking->event->post_id;
            global $wpdb;
            $results = $wpdb->get_results("SELECT ticket_price FROM hnlaw_em_tickets WHERE event_id=$did", OBJECT);
            $output = number_format((float)$results[0]->ticket_price, 2, '.', '');
            $tax_rate = get_option('dbem_bookings_tax');
            $tax = ($tax_rate / 100) * $output;
            $subtotal = $output - $tax;
            $total = $output + $tax;
            if (isset($EM_Booking->booking_meta['coupon'])) {
                if ($EM_Booking->booking_meta['coupon']['coupon_type'] == '%') {
                    $total = $total - $EM_Booking->booking_meta['coupon']['coupon_discount'] / 100 * $total;
                }
                if ($EM_Booking->booking_meta['coupon']['coupon_type'] == '#') {
                    $total = $total - $EM_Booking->booking_meta['coupon']['coupon_discount'];
                }
            }
            $client_name = isset($EM_Booking->booking_meta['registration']['user_name']) ? $EM_Booking->booking_meta['registration']['user_name'] : $EM_Booking->booking_meta['booking']['user_name'];
            $client_mail = isset($EM_Booking->booking_meta['registration']['user_email']) ? $EM_Booking->booking_meta['registration']['user_email'] : $EM_Booking->booking_meta['booking']['user_email'];
            // PUT YOUR HTML IN A VARIABLE
            $response_firstname =  $client_mail;
            $response_email = '';
            $response_reference = '';
            $response_date = date("F j, Y");
            $response_title = 'Re : Event registration invoice';
            $response_items = array();
            $response_items['item1'] = array( 
                'title' => get_the_title($event_id) ,
                'value' => '$'. $output
            ); 
            $response_items['item2'] = array( 
                'title' => 'GST @' . $tax_rate ,
                'value' => '$'. $tax
            );
            $response_items['item2'] = array( 
                'title' => 'Coupon Discount',
                'value' => '$'. $discount
            ); 
            $response_items['total'] = array( 
                    'title' => '&nbsp;' ,
                    'value' => 'Total: $' . $response['transaction']['amount'] 
            ); 
            require  __DIR__.'/email/tpl-invoice.php';

           
            $filename = 'Invoice' . $EM_Booking->booking_id . '.pdf';
            $upload = wp_upload_dir();
            $upload_dir = $upload['basedir'] . '/invoices';
            $fullfilepath = $upload_dir . '/' . $filename;
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, 0700);
            }
            $stylesheet = '' . file_get_contents(get_template_directory_uri() . '/inc/pdf.css') . '';
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->use_kwt = true;
            $mpdf->AddPage('P', //orientation L - landscape, P - portrait
            '', // type = E|O|even|odd|next-odd|next-even
            '', // resetpagenum = 1 - ∞
            '', // pagenumstyle = 1|A|a|I|i
            '', // suppress = on|off|1|0
            3, // margin_left
            3, // margin right
            3, // margin top
            3, // margin bottom
            0, // margin header
            0); // margin footer
            $mpdf->WriteHTML($my_html);
            $mpdf->Output($fullfilepath, 'F');
            $data = chunk_split(base64_encode(file_get_contents($fullfilepath)));
            $sdate = get_post_meta($event_id, '_event_start_date', true);
            $stime = get_post_meta($event_id, '_event_start_time', true);
            $location_id = get_post_meta($event_id, '_location_id', true);
            global $wpdb;
            $results = $wpdb->get_results("SELECT * FROM hnlaw_em_locations WHERE location_id=$location_id", OBJECT);
            $webinar_url = get_field('webinar_key', $event_id);
            if (isset($results) && $results[0]->location_name != '') {
                $locate = esc_attr($results[0]->location_name);
            } else {
                $locate = 'Webinar';
            }
            
            $message = get_field('event_message_body_user', 'option');
            $message = str_replace('{user}', $client_name, $message);
            $message = str_replace('{event}', get_the_title($event_id), $message);
            $message = str_replace('{date}', date("d M Y", strtotime($sdate)), $message);
            $message = str_replace('{time}', $stime, $message);
            $message = str_replace('{location}', $locate, $message);
            if (isset($webinar_url) && $webinar_url != '') {
                $message = $message . '<p style="font-size: 15px;"><strong>IMPORTANT – One final step to complete your Registration</strong></p><p>Your registration is almost complete. Please click on the link below to register with GotoWebinar. You will then be emailed login details for the webinar.</p>' . '<p><a style="text-decoration: underline;" href="https://attendee.gotowebinar.com/register/' . $webinar_url . '"><b> https://attendee.gotowebinar.com/register/' . $webinar_url . '</b></a></p>';
            }
            $message = $message . '<p style="font-size: 15px;"><strong>CHANGES & CANCELLATIONS</strong></p><p>If you need to change or cancel your registration, please call (03) 9670 8200 or email <a href="mailto:info@hnlaw.com.au">info@hnlaw.com.au</a></p> 
                 <p><a href="https://hnlaw.com.au/terms-conditions/">Cancellation policy</a></p>
                 <p>We look forward to seeing you.</p>
                 <table width="600" border="0" style="margin:0px; padding:0px;">
                         <tr>
                             <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164922.png" alt="emailsignlogo" align="left" /></td>
                         </tr>
                         <tr>
                             <td style="margin:0px; padding:0px;"><img src="https://hnlaw.com.au/wp-content/uploads/2019/11/Image20191129164910.png" alt="emailsigncontact" align="left" /></td>
                         </tr>
                         <tr>
                             <td><p style="color:#CCCCCC; font-size:11px; font-family:Arial, Helvetica, sans-serif;">Liability limited by a scheme approved under Professional Standards Legislation.  HNLaw Pty Ltd (ACN 068 367 046) trades as Holley Nethercote Lawyers. This email is intended only for the addressee(s). 
                                  If you are not an intended addressee, you must not use it or take any action in reliance upon it. If you
                                  have received this message in error, please delete this email and contact us on the details above.</p></td>
                         </tr>
                         <tr>
                             <td>&nbsp;</td>
                         </tr>
                  </table>
                 </div>';
            $ticketid = $wpdb->get_results("SELECT * FROM hnlaw_em_tickets WHERE event_id=$did", OBJECT);
            $totalspaces = $ticketid[0]->ticket_spaces;
            $tid = $ticketid[0]->ticket_id;
            $abc = $wpdb->get_results("SELECT * FROM `hnlaw_em_tickets_bookings` WHERE ticket_id=$tid", ARRAY_A);
            $client_mail = isset($EM_Booking->booking_meta['registration']['user_email']) ? $EM_Booking->booking_meta['registration']['user_email'] : $EM_Booking->booking_meta['booking']['user_email'];
            $client_name = isset($EM_Booking->booking_meta['registration']['user_name']) ? $EM_Booking->booking_meta['registration']['user_name'] : $EM_Booking->booking_meta['booking']['user_name'];
            $remaining = $totalspaces - sizeof($abc);
            $headers = 'info@hnlaw.com.au' . "\r\n";
            // $message1 = '<b>Hello Admin,</b><p>
            //       A new booking has been confirmed for the following event:</p>
            //          <div class="event-details">
            //            <p>Event Name:  ' . get_the_title($event_id) . '</p>
            //            <p>Date: ' . $sdate . '</p>
            //            <p>Time: ' . $stime . '</p>
            //            <p>Location:' . $locate . '</p>
            //          </div>
            //      <p>Below are the booking details:</p>
            //      <div class="event-details">
            //            <p>Email:  ' . $client_name . '(' . $client_mail . ')</p>
            //            <p>Booking Id: ' . $EM_Booking->booking_id . '</p>
            //            <p>Amount: $' . $total . '</p>
            //            <p>Spaces Remaining: ' . $remaining . '</p>
            //          </div>
            //      <p>Thank you.</p></div>';
            $message1 = get_field('event_message_body_admin', 'option');
            $message1 = str_replace('{event}', get_the_title($event_id), $message1);
            $message1 = str_replace('{date}', $sdate, $message1);
            $message1 = str_replace('{time}', $stime, $message1);
            $message1 = str_replace('{location}', $locate, $message1);
            $message1 = str_replace('{name}', $client_name, $message1);
            $message1 = str_replace('{email}', $client_mail, $message1);
            $message1 = str_replace('{bookingid}', $EM_Booking->booking_id, $message1);
            $message1 = str_replace('{amount}', $total, $message1);
            $message1 = str_replace('{remaining}', $remaining, $message1);
            // print_r($EM_Booking);
            // echo $client_mail;
            // echo $client_name;
            $mailit = wp_mail($client_mail, 'Event Registration', $message, $headers, $fullfilepath);
            $admin_mail = get_option('admin_email');
            $mailadmin = wp_mail($admin_mail, 'Event Booking', $message1, $headers);
            return;
        }
        global $wpdb;
        
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Content-Type, origin");
        $curl = curl_init();
        curl_setopt_array($curl, array(CURLOPT_URL => "https://sandbox.auth.paymentsapi.io/login", CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => "{\n  \"Username\": 2042.4512,\n  \"Password\": \"d8e3a991-649e-48f3-bd95-d9bff580f150\"\n}", CURLOPT_HTTPHEADER => array("Accept: */*", "Accept-Encoding: gzip, deflate", "Cache-Control: no-cache", "Connection: keep-alive", "Content-Length: 81", "Content-Type: application/json", "Cookie: __cfduid=d9d797653593b1f3febbeca20b1147d031567744154", "Host: sandbox.auth.paymentsapi.io", "Postman-Token: 3e720b50-a00e-4c76-b590-9d2dbd107e43,dae71129-a9f7-4277-8c58-e2ad54dcbb4c", "User-Agent: PostmanRuntime/7.16.3", "cache-control: no-cache"),));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response = json_decode($response, TRUE);
        $abc = $response['access_token'];
        set_transient('bearer_token', $abc, time() + (3600 * 30));
        $extrainfo = 'Event-' . $EM_Booking->event->post_id;
        $def = $this->return_redirect_url_hpp($abc, $EM_Event, $EM_Booking, $extrainfo);
        $def = json_decode($def, TRUE);
        $url = $def['redirectToUrl'];
        echo $url;
        exit();
    }
    
    //function to get return url
    function return_redirect_url_hpp($abc, $EM_Event, $EM_Booking, $extrainfo) {
        if (strpos($extrainfo, 'Event') !== false) {
            global $EM_Event;
        }
        
        $curl = curl_init();
        $arr = json_decode("{\n\n    \"ReturnUrl\": \"" . esc_url(home_url('/')) . "payment-response/\",\n\n    \"Template\": \"Basic\",\n\n    \"Transaction\": {\n\n      \"ProcessType\": \"COMPLETE\",\n\n      \"Reference\": \"\",\n\n      \"Description\": \"Test HPP API Token\",\n\n      \"Amount\": 23.00,\n\n      \"CurrencyCode\": \"AUD\"\n\n    },\n\n    \"Payer\": {\n\n      \"SavePayer\": false,\n\n      \"FamilyOrBusinessName\": \"Surname\",\n\n      \"GivenName\": \"First Name\",\n\n      \"Email\": \"\",\n\n      \"Phone\": \"\",\n\n      \"Mobile\": \"\",\n\n      \"Address\": {\n\n        \"Line1\": \"\",\n\n        \"Line2\": null,\n\n        \"Suburb\": \"\",\n\n        \"State\": \"\",\n\n        \"PostCode\": \"\",\n\n        \"Country\": null\n\n      }\n\n    },\n\n    \"Audit\": {\n\n      \"Username\": \"Token Example\",\n\n      \"UserIP\": \"\"\n\n    }\n\n  }", true);
        $tnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $tnid = 'HPP-TOKEN-' . $tnid;
        foreach ($arr as $key => $xyz) {
            if (is_array($xyz)) {
                foreach ($xyz as $k => $v) {
                    if ($k == 'Reference') {
                        $arr['Transaction']['Reference'] = $tnid;
                    }
                    if ($k == 'Amount') {
                        if (strpos($extrainfo, 'Event') !== false) {
                            $arr['Transaction']['Amount'] = $EM_Booking->booking_price;
                        } else {
                            $arr['Transaction']['Amount'] = $EM_Booking->booking_meta['Transaction']['Amount'];
                        }
                    }
                    $name = explode(' ', $EM_Booking->booking_meta['registration']['user_name']);
                    $arr['Payer']['Address']['Suburb'] = '';
                    $arr['Payer']['GivenName'] =  $name[0]. ' ' . $name[1];
                    $arr['Payer']['FamilyOrBusinessName'] = $EM_Booking->booking_meta['booking']['company_name'];
                    $arr['Payer']['Email'] = $EM_Booking->booking_meta['registration']['user_email'];
                    $arr['Payer']['Mobile'] = $EM_Booking->booking_meta['registration']['dbem_phone'];
                    $arr['Payer']['Address']['Line1'] = $EM_Booking->booking_meta['registration']['dbem_address'];
                    $arr['Payer']['Address']['PostCode'] = $EM_Booking->booking_meta['registration']['dbem_zip'];
                    $arr['Payer']['Address']['Country'] = $EM_Booking->booking_meta['registration']['dbem_country'];
                    $arr['Payer']['Address']['State'] = $EM_Booking->booking_meta['registration']['dbem_state'];
                    $arr['Payer']['Address']['Line2'] = $EM_Booking->booking_meta['registration']['dbem_city'];
                    $arr['Transaction']['Description'] = $extrainfo;
                }
            }
        }
        $arr = json_encode($arr);
        curl_setopt_array($curl, array(CURLOPT_URL => "https://sandbox.rest.paymentsapi.io/businesses/2042/services/tokens/hpp/", CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => $arr, CURLOPT_HTTPHEADER => array("Accept: */*", "Accept-Encoding: gzip, deflate", "Authorization: Bearer " . $abc, "Cache-Control: no-cache", "Connection: keep-alive", "Content-Length: " . strlen($arr), "Content-Type: application/json", "Cookie: __cfduid=d9d797653593b1f3febbeca20b1147d031567744154", "Host: sandbox.rest.paymentsapi.io", "Postman-Token: 5d48bd5b-79de-4ecd-8ef2-34037d83960e,76104046-bd36-4971-a8ff-98b06cb070f0", "User-Agent: PostmanRuntime/7.16.3", "cache-control: no-cache"),));
        $response = curl_exec($curl);

        $err = curl_error($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        if(isset($response['errorCode']))
        {
          echo json_encode($response);
        }
        else{
          return json_encode($response);
        }
        // if ($err) {
        //     return "cURL Error #:" . $err;
        // } else {
        // }
    }

    //function to check the payment info via token
    function return_details_from_token() {
        global $wpdb;
        
        $token = $_GET['webPageToken'];
        $curl = curl_init();
        curl_setopt_array($curl, array(CURLOPT_URL => "https://sandbox.rest.paymentsapi.io/businesses/2042/services/tokens/" . $token, CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 0, CURLOPT_FOLLOWLOCATION => false, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "GET", CURLOPT_HTTPHEADER => array("Accept: */*", "Accept-Encoding: gzip, deflate", "Authorization: Bearer " . get_transient('bearer_token'),),));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            return;
        } else {
            ///////database save///////
            $details = $response;
            $response = json_decode($response, TRUE);
            
            if ($response['transaction']['statusCode'] != 'S' && $response['transaction']['statusCode'] != 'C' && $response['status'] != 'PROCESSED_SUCCESSFUL') {
                echo '<h2>Payment Failed. Please try again.</h2>';
                return;
            }
            // $this->send_email_invoice($response);
            $extrainfo = $response['requestHpp']['transaction']['description'];
            if ( strpos($extrainfo, 'Event') !== false) {
                $this->return_detail_from_token_event($response);
                $this->hn_wp_husbspot_email_register( $response );
                // $this->hn_wp_hubspot( $response );
            } else if (strpos($extrainfo, 'Documents') !== false) {
                $this->return_detail_from_token_documents($response);
            } else if (strpos($extrainfo, 'Webinar') !== false) {
                $this->return_detail_from_token_webinar($response);
            } else if (strpos($extrainfo, 'Course') !== false) {
                $this->return_detail_from_token_course($response);
            } else if (strpos($extrainfo, 'Package') !== false) {
                $this->return_detail_from_token_package($response);
            }
            $this->webinar_callback($response);
        } /*else*/
    }

    function return_detail_from_token_event(&$response) {
        global $wpdb;
        $extrainfo = $response['requestHpp']['transaction']['description'];
        $desc = explode("-", $extrainfo);
        $description = $desc[0];
        $did = $desc[1];
        $sdate = get_post_meta($desc[1], '_event_start_date', true);
        $stime = get_post_meta($desc[1], '_event_start_time', true);
        $etime = get_post_meta($desc[1], '_event_end_time', true);
        $eid = get_transient('bookingid');
        $sql = "UPDATE `hnlaw_em_bookings` SET `booking_status` = 1 WHERE `hnlaw_em_bookings`.`booking_id` = {$eid};";
        $results = $wpdb->get_results($sql);
        $table_name = 'hnlaw_payment';
        $wpdb->insert($table_name, array('payerid' => $response['transaction']['payer']['payerId'], 'amount' => $response['transaction']['amount'], 'email' => $response['requestHpp']['payer']['email'], 'currency' => $response['transaction']['currency'], 'method' => $response['transaction']['paymentMethod'], 'card' => $response['transaction']['card']['cardNumber'], 'description' => 'Event', 'did' => $did, 'details' => $description ), array('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s'));
        if (strpos($extrainfo, 'Event') !== false) {
            $desc = explode("-", $extrainfo);
            $description = $desc[0];
            $did = $desc[1];
            $eid = get_transient('bookingid');
            $sql = "UPDATE `hnlaw_em_bookings` SET `booking_status` = 1 WHERE `hnlaw_em_bookings`.`booking_id` = {$eid};";
            $results = $wpdb->get_results($sql);
            $table_name = 'hnlaw_payment';
            $wpdb->insert($table_name, array('payerid' => $response['transaction']['payer']['payerId'], 'amount' => $response['transaction']['amount'], 'email' => $response['requestHpp']['payer']['email'], 'currency' => $response['transaction']['currency'], 'method' => $response['transaction']['paymentMethod'], 'card' => $response['transaction']['card']['cardNumber'], 'description' => 'Event', 'did' => $did, 'details' => $description), array('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s'));
        }
          $this->send_email_invoice($response);
          ?>
            <div class="event-payment-details">
                <p><?php echo get_field('events_purchase', 'option'); ?></p>
                <div class="event-details">
                     <p><b>Event Name:</b>  <?php echo get_the_title($desc[1]); ?></p>
                     <p><b>Date:</b> <?php echo $sdate; ?></p>
                     <p><b>Time: </b><?php echo $stime.' - '.$etime; ?></p>
                     <p><b>Location: </b>
                     <?php
                      $location_id = get_post_meta($desc[1], '_location_id', true);
                      $results = $wpdb->get_results("SELECT * FROM hnlaw_em_locations WHERE location_id=$location_id", OBJECT);
                      if (isset($results) && $results[0]->location_name != '') {
                          echo esc_attr($results[0]->location_name);
                      } else {
                          echo 'Webinar';
                      }
                      $this->webinar_callback($response);
                      ?>
                    </p>
                </div>
                <p>
                  You will receive two separate emails - An invoice, along with confirmation details of your registration and a confirmation of event payment.
                </p>
                <p>
                  Please note:  Unfortunately, some booking confirmations seem to be ending up in junk folders.  If you do not receive your confirmation of registration email, please check if it might have ended up there.  
                </p>
                <p>Visit the Event: <a href="<?php echo get_permalink($desc[1]);?>"><?php echo get_the_title($desc[1]);?></a></p>
            </div>
        <?php
        $dateStart = date("Ymd", strtotime($sdate));
        $dateEnd = date("Ymd");
        $edate = get_post_meta($desc[1], '_event_end_date', true);
        if (isset($edate) && $edate != '') {
            $dateEnd = date("Ymd", strtotime($edate));
        }
        if (get_option('dbem_timezone_enabled')) {
            $event_timezone = get_option('dbem_timezone_default');
            if ($event_timezone == 'UTC+0' || $event_timezone == 'UTC +0') {
                $event_timezone = 'UTC';
            }
        } else {
            $event_timezone = EM_DateTimeZone::create()->getName(); //set a default timezone if none exists
            
        }
        $gcal_url = 'http://www.google.com/calendar/event?action=TEMPLATE&text=event_name&dates=start_date/end_date&details=post_content&location=location_name&trp=false&sprop=event_url&sprop=name:blog_name&ctz=event_timezone';
        $gcal_url = str_replace('event_name', get_the_title($desc[1]), $gcal_url);
        $gcal_url = str_replace('start_date', urlencode($dateStart), $gcal_url);
        $gcal_url = str_replace('end_date', urlencode($dateEnd), $gcal_url);
        $gcal_url = str_replace('location_name', $results[0]->location_name, $gcal_url);
        $gcal_url = str_replace('blog_name', urlencode(get_bloginfo()), $gcal_url);
        $gcal_url = str_replace('event_url', urlencode(get_permalink($desc[1])), $gcal_url);
        $gcal_url = str_replace('event_timezone', urlencode($event_timezone), $gcal_url);
        $event = get_post($desc[1]);
        //calculate URL length so we know how much we can work with to make a description.
        if (!empty($event->post_excerpt)) {
            $gcal_url_description = $event->post_excerpt;
        } else {
            $matches = explode('<!--more', $event->post_content);
            $gcal_url_description = wp_kses_data($matches[0]);
        }
        $gcal_url_length = strlen($gcal_url) - 9;
        if (strlen($gcal_url_description) + $gcal_url_length > 1350) {
            $gcal_url_description = substr($gcal_url_description, 0, 1380 - $gcal_url_length - 3) . '...';
        }
        $gcal_url = str_replace('post_content', urlencode($gcal_url_description), $gcal_url);
        //get the final url
        $replace = $gcal_url;
        $img_url = 'www.google.com/calendar/images/ext/gc_button2.gif';
        $img_url = is_ssl() ? 'https://' . $img_url : 'http://' . $img_url;
        $replace = '<a href="' . esc_url($replace) . '" target="_blank"><img src="' . esc_url($img_url) . '" alt="0" border="0"></a>';
        echo $replace;
        /**
         * iCalendar
         */
        $param = array('summary' => addslashes(get_the_title($desc[1])), 'dtstart' => $dateStart, 'dtend' => $dateEnd, 'location' => addslashes($results[0]->location_name), 'url' => get_permalink($desc[1]), 'uid' => date('Y-m-d-H-i-s') . "@" . get_home_url(), // unique ID,
        'description' => addslashes($gcal_url_description),);
        // error_log(  print_r($param,1) );
        $this->add_event_to_icalendar($param, $response['requestHpp']['payer']['email']);
    }

    function return_detail_from_token_documents(&$response) {
        global $wpdb;
        $extrainfo = $response['requestHpp']['transaction']['description'];
        $cart_contents = edd_get_cart_contents();
        
        if (!empty($cart_contents)) {
            foreach ($cart_contents as $item => $itemid) {
              $terms = get_the_terms($itemid['id'], 'download_category');
              if( $terms &&  !is_wp_error( $terms ) ) {
                foreach ($terms as $term) {
                  delete_term_meta($term->term_id, 'item_in_cart');
                  $extrainfo1 = substr($extrainfo, strpos($extrainfo, "-") + 1);
                  if (strpos($extrainfo1, ',') !== false) {
                    $extrainfo1 = rtrim($extrainfo1, ',');
                    $extrainfo1 = explode(',', $extrainfo1);
                    foreach ($extrainfo1 as $extra) {
                      $table_name = 'hnlaw_payment';
                      $res = $wpdb->insert(
                          $table_name, 
                          array(
                            'payerid' => null, 
                            'amount' => $response['transaction']['amount'], 
                            'email' => $response['requestHpp']['payer']['email'], 
                            'currency' => $response['transaction']['currency'], 
                            'method' => $response['transaction']['paymentMethod'], 
                            'card' => $response['transaction']['card']['cardNumber'], 
                            'description' => 'Document', 
                            'did' => $extra, 
                            'details' => json_encode($response)
                          ), 
                          array(
                            '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s'
                          )
                      );
                    }
                  }
                }    
              }
            }
            $name = explode(' ', $response['requestHpp']['payer']['givenName']);
            $user_info['email'] = $response['requestHpp']['payer']['email'];
            $user_info['first_name'] = $name[0];
            $user_info['last_name'] = $name[1];
            $user_info['id'] = get_current_user_id();
            $user_info['address'] = array('line1' => $response['requestHpp']['payer']['address']['line1'], 'line2' => $response['requestHpp']['payer']['address']['line2'], 'city' => $response['requestHpp']['payer']['address']['suburb'], 'zip' => $response['requestHpp']['payer']['address']['postCode'], 'country' => $response['requestHpp']['payer']['address']['country'], 'state' => $response['requestHpp']['payer']['address']['state']);
            foreach ($extrainfo1 as $extra) {
                // $cartdetail['id'] = $extra;
                $cartdetail[] = array('id' => $extra, ' ' => 1,
                // 'price_id'   => $response['transaction']['amount'],
                'tax' => $item['tax'], 'item_price' => $response['transaction']['amount'], 'price' => $response['transaction']['amount'], 'fees' => isset($item['fees']) ? $item['fees'] : array(), 'discount' => isset($item['discount']) ? $item['discount'] : 0, 'item_number' => array('options' => array('price_id' => $response['transaction']['amount'])));
            }
            $arr = json_decode($response['transaction']['description'], true);
            $arr['quantity'] = 1;
            
            $payment_data = array(
                'price' => $response['transaction']['amount'], 
                'date' => date('Y-J-d'), 
                'user_email' => $response['requestHpp']['payer']['email'], 
                'purchase_key' => $response['transaction']['reference'],
                'currency' => 'USD', 
                'downloads' => $arr, 
                'user_info' => $user_info, 
                'cart_details' => $cartdetail, 
                'status' => 'publish',
            // 'address'     => $address
            );
            // Record the pending payment
            $payment = edd_insert_payment($payment_data);
            // print_r($payment);
            // if ( $payment ) {
            edd_update_payment_status($payment, 'publish');
            // Empty the shopping cart
            edd_empty_cart();
            $customer_id = get_current_user_id();
            $customer = new EDD_DB_Customers($customer_id);
            update_post_meta($payment, '_edd_payment_gateway', 'integrapay');
            update_post_meta($payment, '_edd_payment_total', $response['transaction']['amount']);
            $purchase_value = $response['transaction']['amount'];
            global $wpdb;
            $customer->increment_stats( $customer_id,$purchase_value );
            
            $current_user = wp_get_current_user();
            $uid = $current_user->ID;
            $documents = get_user_meta($uid, 'documents', true);
            $extrainfo = $response['transaction']['description'];
            if (isset($documents) && $documents != '') {
                $extrainfo = substr($extrainfo, strpos($extrainfo, "-") + 1);
                $extrainfo = rtrim($extrainfo, ',');
                $arr = explode(',', $extrainfo);
                $arr_merge = array_merge($documents, $arr);
                $arr_merge = array_unique($arr_merge);
            } else {
                $extrainfo = substr($extrainfo, strpos($extrainfo, "-") + 1);
                $extrainfo = rtrim($extrainfo, ',');
                $arr_merge = explode(',', $extrainfo);
                $arr_merge = array_unique($arr_merge);
            }
            update_user_meta($uid, 'documents', $arr_merge);
            $this->send_email_invoice($response);
            echo get_field('documents_purchase','option');
        }
    }

    function return_detail_from_token_webinar(&$response) {
        $extrainfo = $response['requestHpp']['transaction']['description'];
        global $wpdb;
        $current_user = wp_get_current_user();
        $uid = $current_user->ID;
        $webinars = get_user_meta($uid, 'webinars', true);
        $desc = explode("-", $extrainfo);
        $description = $desc[0];
        $did = $desc[1];
        if (isset($webinars) && $webinars != '') {
            $arr[] = $did;
            $arr_merge = array_merge($webinars, $arr);
        } else {
            $arr_merge[] = $did;
        }
        $this->send_email_invoice($response);
        update_user_meta($uid, 'webinars', $arr_merge);
        $desc = explode("-", $extrainfo);
        $description = $desc[0];
        $did = $desc[1];
        $eid = absint($response['transaction']['description']);
        echo get_field('webinar_purchase','option');
        echo '<p>Visit the Webinar: <a href="'.get_permalink($did).'">'.get_the_title($did).'</a></p>';
    }

    function return_detail_from_token_package(&$response) {
        $extrainfo = $response['requestHpp']['transaction']['description'];
        $desc = explode("-", $extrainfo);
        $description = $desc[0];
        $did = $desc[1];
        $username = $response['requestHpp']['payer']['email'];
        $password = wp_generate_password(8, false);
        $email_address = $response['requestHpp']['payer']['email'];
        if ( !get_user_by( 'email', $email_address ) ) {
            $user_id = wp_create_user($username, $password, $email_address);
            $user = new WP_User($user_id);
            $user->set_role('subscription_admin');
            $order_id = get_transient('order_id');
            $sub_user_count = get_post_meta($order_id, 'users__number_of_users', true);
            

            $name = explode(' ', $response["requestHpp"]["payer"]["givenName"] ) ;
            if( count( $name ) > 1 ) {
                /**break */
                update_user_meta( $user_id, 'first_name', $name[0] );
                unset( $name[0] );

                update_user_meta( $user_id, 'last_name',join(' ', $name ) );
            } else {

                update_user_meta( $user_id, 'first_name',  $response["requestHpp"]["payer"]["givenName"] );
                # update_user_meta( $user_id, 'last_name',$response["requestHpp"]["payer"]["familyOrBusinessName"] );

            }

            update_user_meta($user_id, 'sub_user_count', $sub_user_count);
            update_user_meta($user_id, 'payment_details', $response);
            update_user_meta( $user_id, 'user_status', 'active' );
            update_user_meta( $user_id, 'phone', $response['requestHpp']['payer']['phone'] );
            update_user_meta( $user_id, 'phone', $response['requestHpp']['payer']['mobile'] );
            
            update_user_meta( $user_id, 'subscribe_regulatory_updates', 'yes' );

            
            //$message = get_field('package_message_login_body_user','option');
            $message = get_field('package_subscription_successful_message_to_sub_user','option' );
            
            $message = str_replace('{user}', $response["requestHpp"]["payer"]["givenName"], $message);
            $message = str_replace('{username}', $username, $message);
            $message = str_replace('{password}', $password, $message);
            $message = str_replace('{login_url}', home_url('/sign-in'), $message);
            $message = str_replace('{package}', get_the_title($did), $message);

            //send email
            $headers = 'FROM: info@hnlaw.com.au' . "\r\n";
            $mailit = wp_mail( $email_address, get_field('package_subscription_message_subject_to_sub_user', 'option'), $message, $headers);
            $user_meta = get_user_meta( $user_id );
            $user_data = get_userdata( $user_id );
            $user_role = end( $user_data->roles );
            if( $user_role == 'freeuser' ){
              $u = new WP_User( $user_id );
              $u->set_role( 'subscription_admin' );
              wp_redirect( home_url('/account-details') ); exit;
            }
            //send email
        } else {
            $user = get_user_by( 'email', $email_address );
            $user_id = $user->ID;
            
            if( is_user_logged_in() )
            {
                $current_user = wp_get_current_user();
                $user_id = $current_user->ID;
            }
            
            update_user_meta( $user_id, 'subscribe_regulatory_updates', 'yes' );
            $order_id = get_transient('order_id');
            $sub_user_count = get_post_meta($order_id, 'users__number_of_users', true);
            update_user_meta($user_id, 'sub_user_count', $sub_user_count);
            update_user_meta($user_id, 'payment_details', $response);

            update_user_meta( $user_id, 'phone', $response['requestHpp']['payer']['phone'] );
            update_user_meta( $user_id, 'phone', $response['requestHpp']['payer']['mobile'] );

            //send email
            $message = get_field('package_message_body_user', 'option');
            $message = str_replace('{user}', $response["requestHpp"]["payer"]["givenName"], $message);
            $message = str_replace('{package}', get_the_title($did), $message);
            $message = str_replace('{amount}', '$'.$response['transaction']['amount'], $message);

            $headers = 'info@hnlaw.com.au' . "\r\n";
            
            // $mailit = wp_mail( $email_address, get_field('package_subject_user', 'option'), $message, $headers);
            
            $user_meta = get_user_meta( $user_id );

            $user_data = get_userdata( $user_id );
            $user_role = end( $user_data->roles );
            if( $user_role == 'freeuser' ){
              $u = new WP_User( $user_id );
              $u->set_role( 'subscription_admin' );
              wp_redirect( home_url('/account-details') ); exit;
            }
            //send email
        }
        
        $packages = get_user_meta($user_id, 'packages', true);
        if (isset($packages) && $packages != '') {
            $arr[] = $did;
            $arr_merge = array_merge($packages, $arr);
        } else {
            $arr_merge[] = $did;
        }
        update_user_meta($user_id, 'packages', $arr_merge);
        
        $orders = get_user_meta($user_id, 'orders', true);
        if (isset($orders) && $orders != '') {
            $arr[] = get_transient('order_id');
            $arr_merge = array_merge($orders, $arr);
        } else {
            $arr_merge[] = get_transient('order_id');
        }
        update_user_meta($user_id, 'orders', $arr_merge);
        $oid = get_transient('order_id');
        $key_contact_name = get_post_meta($oid, 'key_contact__name', true);
        $key_contact_phone = get_post_meta($oid, 'key_contact__phone', true);
        update_user_meta($user_id, 'key_contact__name', $key_contact_name);
        update_user_meta($user_id, 'key_contact__phone', $key_contact_phone);
        global $wpdb;
        $table_name = $wpdb->prefix . 'payment';
        
        $success = $wpdb->insert($table_name, 
          array(
            'payerid' => $response['transaction']['payer']['payerId'], 
            'amount' => $response['transaction']['amount'], 
            'email' => $response['requestHpp']['payer']['email'], 
            'currency' => $response['transaction']['currency'], 
            'method' => $response['transaction']['paymentMethod'], 
            'card' => $response['transaction']['card']['cardNumber'], 
            'description' => $description, 
            'did' => $did, 
            'details' => json_encode( $response ) 
          ), 
          array(
            '%d', //payerid
            '%s', //amount
            '%s', //email
            '%s', // currency
            '%s', //method
            '%s', //card
            '%s', //description
            '%s', //did
            '%s', //details
          ));
        echo get_field('package_subscription','option');
        $this->send_email_invoice($response);
        wp_update_post(array('ID' => $oid, 'post_status' => 'publish'));
    }

    function return_detail_from_token_course(&$response) {
        $extrainfo = $response['requestHpp']['transaction']['description'];
        $current_user = wp_get_current_user();
        $uid = $current_user->ID;
        $courses = get_user_meta($uid, 'courses', true);
        $desc = explode("-", $extrainfo);
        $description = $desc[0];
        $did = $desc[1];
        if (isset($courses) && '' != $courses) {
            $arr[] = $did;
            $arr_merge = array_merge($courses, $arr);
        } else {
            $arr_merge[] = $did;
        }
        $this->send_email_invoice($response);
        /**
         * update in course post meta
         */
        ld_update_course_access($uid, $did);
        /**
         * update user meta
         */
        if (function_exists('hn_ln_enroll_user_to_course')) {
            hn_ln_enroll_user_to_course($uid, $did);
        } else {
            $enroll_timestamp_gmt = time();
            $user_course_access_time = get_user_meta($uid, "course_" . $did . "_access_from", true);
            if (empty($user_course_access_time)) {
                update_user_meta($uid, "course_" . $did . "_access_from", $enroll_timestamp_gmt);
            }
        }
        update_user_meta($uid, 'courses', $arr_merge);
        /**
         * update transaction
         */
        global $ld_lms_processing_id;
        $ld_lms_processing_id = time();
        global $ipn_log_filename;
        $ipn_log_filename = '';
        $wp_upload_dir = wp_upload_dir();
        $ld_ipn_logs_dir = trailingslashit($wp_upload_dir['basedir']) . 'learndash/paypal_ipn/';
        $ipn_log_filename = trailingslashit($ld_ipn_logs_dir) . $ld_lms_processing_id . '.log';
        if (!file_exists($ld_ipn_logs_dir)) {
            if (wp_mkdir_p($ld_ipn_logs_dir) == false) {
                $ipn_log_filename = '';
            }
        }
        @file_put_contents(trailingslashit($ld_ipn_logs_dir) . 'index.php', '// nothing to see here');
        $this->hn_ld_ipn_debug(print_r($response, true));
        $user_id = $uid;
        $course_id = $did;
        $email = $response['requestHpp']['payer']['email'];
        $usermeta = get_user_meta($user_id, '_sfwd-courses', true);
        if (empty($usermeta)) {
            $usermeta = $course_id;
        } else {
            $usermeta.= ",$course_id";
        }
        update_user_meta($user_id, '_sfwd-courses', $usermeta);
        $transaction = json_decode($response['transaction'], true);
        $transaction['user_id'] = $user_id;
        $transaction['course_id'] = $course_id;
        $transaction['log_file'] = basename($ipn_log_filename);
        $course_title = '';
        $course = get_post($course_id);
        if (!empty($course)) {
            $course_title = $course->post_title;
        }
        $this->hn_ld_ipn_debug('Course Title: ' . $course_title);
        $post_id = wp_insert_post(array('post_title' => "Course {$course_title} Purchased By {$email}", 'post_type' => 'sfwd-transactions', 'post_status' => 'publish', 'post_author' => $user_id));
        $this->hn_ld_ipn_debug('Created Transaction. Post Id: ' . $post_id);
        foreach ($transaction as $k => $v) {
            update_post_meta($post_id, $k, $v);
        }
        echo get_field('course_purchase','option');
        echo "<p><a href='" . get_permalink($course_id) . "'>Get Back to Course Page</a></p>";
    }

    function hn_ld_ipn_debug($msg) {
        global $ld_lms_processing_id, $ipn_log_filename;
        if (!empty($ipn_log_filename)) {
            file_put_contents($ipn_log_filename, learndash_adjust_date_time_display(time(), 'Y-m-d H:i:s') . " [" . $ld_lms_processing_id . "] " . $msg . "\r\n", FILE_APPEND);
        }
    }


    function add_event_to_icalendar($data, $user_email) {
        require_once plugin_dir_path(__FILE__) . '../icalendar-lib/zapcallib.php';
        $icalobj = new ZCiCal();
        // create the event within the ical object
        $eventobj = new ZCiCalNode("VEVENT", $icalobj->curnode);
        if ($data) {
            foreach ($data as $key => $val) {
                if ('dtstart' == $key || 'dtend' == $key) {
                    // error_log( $key );
                    $eventobj->addNode(new ZCiCalDataNode(strtoupper($key) . ":" . $val));
                } else {
                    $eventobj->addNode(new ZCiCalDataNode(strtoupper($key) . ":" . $val));
                }
            }
        }
        // DTSTAMP is a required item in VEVENT
        $eventobj->addNode(new ZCiCalDataNode("DTSTAMP:" . ZCiCal::fromSqlDateTime()));
        $upload_dir = wp_upload_dir();
        if (!is_dir($upload_dir['basedir'] . '/ics/')) {
            @mkdir($upload_dir['basedir'] . '/ics/');
        }
        $filename = '/ics/event-' . time() . '.ics';
        $file = fopen($upload_dir['basedir'] . $filename, "w");
        fwrite($file, $icalobj->export());
        fclose($file);
        echo "<br />";
        //echo '<a href="'.$upload_dir['baseurl']. $filename . '" download >Add Event reminder</a>';
        echo "<br /> ";
        echo '<input type="email" name="event-remainder-email" id="event-remainder-email" value="' . $user_email . '" />';
        echo '<a href="#" class="btn" style="margin-left:5px" id="event-remainder-email-send" data-event="' . esc_url($filename) . '" >' . esc_html__('Send Event reminder via Email', 'hn-law') . '</a>';
        echo '<div id="email-response"></div>';
    }


    function hn_hub_integra_send_ics_to_mail() {
        $file_path = $_POST['ics'];
        $email = $_POST['email'];
        if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $upload_dir = wp_upload_dir();
            if (file_exists($upload_dir['basedir'] . $file_path)) {
                $headers = array();
                $headers[] = 'FROM: Hn Law <info@hnlaw.com.au>';
                $attachments = array($upload_dir['basedir'] . $file_path);
                $subject = 'HN Law | Event Reminder';
               
                $response_firstname = '';
                $response_email = $email;
                $response_reference = '';
                $response_date = date("F j, Y");
                $response_title = 'Event Reminder';
                $response_items = array();
                $response_message = 'Please find the attachment.';
                require  __DIR__.'/email/tpl-invoice.php';

                
                if (wp_mail($email, $subject, $my_html, $headers, $attachments)) {
                    echo wp_send_json_success(array('message' => 'Attachment has been send via email'));
                } else {
                    echo wp_send_json_error(array('message' => 'Unable to send email. Please try again'));
                }
            } else {
                echo wp_send_json_error(array('message' => 'ICS file not found. Please contact our webmasters'));
            }
        } else {
            echo wp_send_json_error(array('message' => 'Invalid Email ID'));
        }
    }


    function webinar_callback($response) {
        list( $result, $error ) = $this->get_oauth_access_token_from_api_getgo();

        if (!$error) {
            if ($result) {
                $response1 = json_decode($result, TRUE);
                if (isset($response1['access_token'])) {
                    $authorization_key = $response1['access_token'];
                    $organizer_key = $response1['organizer_key'];
                    $extrainfo = $response['requestHpp']['transaction']['description'];
                    if (false !== strpos($extrainfo, 'Event')) {
                        $desc = explode("-", $extrainfo);
                        $event_id = $desc[1]; //event id
                        $webinar_key = get_field('webinar_key', $event_id);
                        $description = $desc[0];
                        $endpoint = sprintf("https://api.getgo.com/G2W/rest/v2/organizers/%s/webinars/%s/registrants?resendConfirmation=false", $organizer_key, $webinar_key);
                        $args = array('method' => 'POST',
                        // 'timeout'     => 90,
                        'sslverify' => false, 'headers' => array('Authorization' => 'Bearer ' . $authorization_key, 'Accept' => 'application/vnd.citrix.g2wapi-v1.1+json', 'Content-Type' => 'application/x-www-form-urlencoded',), 'body' => json_encode(array("firstName" => $response['requestHpp']['payer']['givenName'], "lastName" => $response['requestHpp']['payer']['familyOrBusinessName'], "email" => $response['requestHpp']['payer']['email'])),);
                        $result = $this->hn_wp_remote_request($endpoint, $args);
                        if (!is_wp_error($result) || wp_remote_retrieve_response_code($result) == 200) {
                        } else {
                            $error_message = $result->get_error_message();
                            echo $result;
                        }
                    }
                } else {
                    echo "Unable to regenerate access token";
                }
            }
        } else {
            echo "Error" . $error;
        }
    }

    function hn_wp_remote_request($endpoint, $args) {
        $response = wp_remote_request($endpoint, $args);
        return $response;
    }

    function hn_wp_husbspot_email_register( $response ) {
        return;
        $curl = curl_init();
        $hubspot_key = '5c877447-5724-49f5-b3e0-3861c77f4656';
        $data = array(
            'properties' => array(
                array(
                    'properties' => 'email',
                    'value' =>  $response['requestHpp']['payer']['email']
                ),
                array(
                    'properties' => 'firstname',
                    'value' => $response["requestHpp"]["payer"]["givenName"]
                ),
                array(
                    'properties' => 'lastname',
                    'value' =>  ''
                ),
                array(
                    'properties' => 'phone',
                    'value' =>  $response['requestHpp']['payer']['phone']
                ),
            )
        );

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => "https://api.hubapi.com/contacts/v1/contact?hapikey=" . $hubspot_key,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            )
        );
        $curl_response = curl_exec($curl);
        if ( false === $curl_response ) {
            echo 'ERR' . curl_error($curl);
        } else {
            
            echo $curl_response;
        }
        
        curl_close($curl);
    }

    function hn_wp_hubspot( $response ) {
        //Process a new form submission in HubSpot in order to create a new Contact.

        $hubspotutk      = 'hn website hubspots';//$_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
        $ip_addr         = $_SERVER['REMOTE_ADDR']; //IP address too.
        $hs_context      = array(
            'hutk' => $hubspotutk,
            'ipAddress' => $ip_addr,
            'pageUrl' => home_url(),
            
            'pageName' => 'Home'
        );
        $hs_context_json = json_encode($hs_context);
        
        //Need to populate these variable with values from the form.
        $str_post = "firstname=" . urlencode( $response['requestHpp']['payer']['givenName'] )
            . "&lastname=" . urlencode( $response['requestHpp']['payer']['familyOrBusinessName'] )
            . "&email=" . urlencode( $response['requestHpp']['payer']['email'] )
            . "&phone=" 
            . "&company="
            . "&hs_context=" . urlencode($hs_context_json); //Leave this one be
        
        //replace the values in this URL with your portal ID and your form GUID
        $endpoint = 'https://forms.hubspot.com/uploads/form/v2/7028269/f161392a-58ba-42a1-90c4-337b562a3b6b';
        
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        @curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response    = @curl_exec($ch); //Log the response from HubSpot as needed.
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
        @curl_close($ch);
        // echo $status_code . " " . $response;
    }
}
#class ends
$obj = new Hn_Hub_Integra_Handle_Payment;
$obj->init();
add_action('init', 'hn_run_init', 1);
// wp
function hn_run_init() {
    if ('GET' == $_SERVER['REQUEST_METHOD'] && isset($_GET['code'])) {
        set_transient('code', $_GET['code']);
        error_log('init run', $_GET['code']);
    }
}
# add_filter( 'http_request_timeout', 'wp9838c_timeout_extend' );
function wp9838c_timeout_extend($time) {
    // Default timeout is 5
    return 50;
}
add_filter('http_request_redirection_count', 'SetDirectionCount');
function SetDirectionCount() {
    return 5;
}
function __set_curl_nofollow(&$handle) {
    curl_setopt($handle, CURLOPT_FOLLOWLOCATION, FALSE);
}
// add_action( 'http_api_curl', '__set_curl_nofollow' );

