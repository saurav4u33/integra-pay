<?php

/**
 * Fired during plugin activation
 *
 * @link       hnhub.com
 * @since      1.0.0
 *
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/includes
 * @author     HN Hub <hnhub@test.com>
 */
class Hn_Hub_Integra_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE `{$wpdb->base_prefix}payment` (
		pid bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT,
		payerid bigint(255) UNSIGNED NULL,
		amount varchar(255),
		email varchar(255),
		currency varchar(255),
		method varchar(255),
		card varchar(255),
		details text(2000),
		description varchar(255),
		did bigint(255),
		PRIMARY KEY  (pid)
		) $charset_collate;";

		dbDelta( $sql );

		return true;
	}
}
