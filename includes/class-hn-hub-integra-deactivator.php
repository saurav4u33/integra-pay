<?php

/**
 * Fired during plugin deactivation
 *
 * @link       hnhub.com
 * @since      1.0.0
 *
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Hn_Hub_Integra
 * @subpackage Hn_Hub_Integra/includes
 * @author     HN Hub <hnhub@test.com>
 */
class Hn_Hub_Integra_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
